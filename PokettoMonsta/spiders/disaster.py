import scrapy
from PokettoMonsta.items import PokettoMonstaItem
from urllib.parse import urljoin


class DisasterSpider(scrapy.Spider):
    name = "disaster"
    start_urls = [
            'http://limitlesstcg.com/decks/?time=3months&format=standard', ]

    def parse(self, response):
        for decks in response.css('td:nth-of-type(2)'):
            link = decks.css('a::attr("href")').extract_first()
            link = urljoin(response.url, link)
            yield scrapy.Request(link, callback=self.parse_def)

    def parse_def(self, response):
        for playerDeck in response.css('td:nth-of-type(7) a'):
            link = playerDeck.css('a::attr("href")').extract_first()
            link = urljoin(response.url, link)
            yield scrapy.Request(link, callback=self.parse_def_nanodata)
            print(link)

    def parse_def_nanodata(self, response):
        for nanoDeck in response.css('div[class="decklist"]'):
            link = nanoDeck.css('a:nth-of-type(1)::attr("href")').extract_first()
        
        